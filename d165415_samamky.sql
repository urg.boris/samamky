-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: wm77.wedos.net:3306
-- Generation Time: Mar 12, 2018 at 06:57 PM
-- Server version: 5.6.17
-- PHP Version: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `d165415_samamky`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` text COLLATE utf8_czech_ci NOT NULL,
  `takeAway` text COLLATE utf8_czech_ci,
  `note` text COLLATE utf8_czech_ci,
  `price` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `equipment` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `registerLink` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `quote` text COLLATE utf8_czech_ci,
  `quotator` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`, `url`, `description`, `takeAway`, `note`, `price`, `equipment`, `registerLink`, `quote`, `quotator`) VALUES
(2, 'Kouzlo keramického mozaikování', 'kouzlo-keramickeho-mozaikovani', 'Kouzlo keramické mozaiky\r\nChcete se naučit univerzální techniku, pomocí které unikátně oživíte kusy nábytku, stěnu vašeho příbytku, podlahu na balkóně, kuchyň, keramické květináče či jiné doplňky? Mozaikování je spojení prožitku z tvorby s estetickým a funkčním výsledkem. Naučte se tuto krásnou techniku a pronikněte do tajů, které její použití skýtá. \r\n \r\nV kurzu si:\r\npopovídáme o dostupných nástrojích a kde je sehnat\r\nnaučíme se s nástroji zacházet\r\nvyzkoušíme různé způsoby vytváření střepů z obkladů\r\npovíme si o různých způsobech lepení a dostupných lepidlech\r\nvyzkoušíme si jak správně vybrat a připravit spárovací hmotu \r\nprovedeme spárování na vysněném výrobku\r\n', 'Techniku si vyzkoušíme na přípravě kachlové podložky pod hrnec. \r\n \r\nPro druhý výrobek si můžete vybrat z mozaikování na květník, tác nebo si rovnou vyrobit samostatný mozaikový obraz (třeba číslo popisné vašeho příbytku). Výrobky s vámi samozřejmě hned poputuje domů :)', NULL, '700,-', 'Pití a svačinku', 'todo', NULL, NULL),
(3, 'Beads mozaikování', 'beads-mozaikovani', 'Beads mozaikování je luxusní moderní technika, jejímž základním prvkem je využití různorodých skleněných korálků, knoflíků a kabošonů. Tato technika je pracnější než klasické keramické mozaikování, práce je celkově jemnější a detailnější, pinzeta se tak stane vaším nejdůvěrnějším společníkem.\r\n \r\nV kurzu se:\r\nDozvíte, jaké materiály lze pro Beads mozaiku použít\r\nPodle čeho je vybírat a kde nakupovat\r\nJak efektivně pracovat s malými korálky, knoflíky a kabošony\r\nJaká jsou nejvhodnější lepidla k lepení korálků\r\nTechniky spárování připraveného základu\r\nCo vše se dá omozaikovat :)', 'Absolvování kurzu vám poskytne veškeré informace potřebné k plnému zvládnutí této výjimečné techniky. Kromě toho, si odnesete omozaikované malé zrcadlo, šperkovnici či vázu. To, do čeho se společně pustíme je na vašich preferencích. Veškerý materiál je zahrnut v ceně kurzu. ', 'Kurz je celodenní s hodinovou pauzou na oběd. ', '2500,- individuálně a 2100,- při počtu 2 a více účastníků', 'Pití, oběd, svačinku a dobrou náladu', 'todo', NULL, NULL),
(4, 'Mikromozaika', 'mikromozaika', 'Přijďte si po práci trochu odfrknout a odneste si nový poklad!\r\n \r\nKurz mikromozaiky je “ochutnávkou” beads mozaikování. Přijďte si vyzkoušet, jak jemná je práce při mozaikování s korálky a vytvořte dechberoucí šperk pro sebe či své milé (brož, náušnice, náhrdelník, náramek, prsten).\r\n \r\nNa kurzu se dozvíte:\r\nJaké materiály lze pro mikromozaiku použít.\r\nPodle čeho je vybírat a kde nakupovat.\r\nJak efektivně pracovat s malými korálky, knoflíky a kabošony.\r\nNejvhodnější lepidla k lepení korálků.\r\nTechniky spárování připraveného základu.\r\nCo vše se dá omozaikovat :)', '', NULL, '600,-', 'Pití, oběd, svačinku a dobrou náladu', 'todo', NULL, NULL),
(5, 'Panenkování s Viviánou na liáně', 'panenkovani-s-vivianou-na-liane', 'V rámci panenkování s Vivianou na liáně si každý vyrobí svou osobní panenku, panáčka, hrdinu či hrdinku. Děti i dospělí si v ateliéru, kde je vřelá a přátelská atmosféra, užívají doslova každou minutu. Celé 4 hodiny se věnují velmi zapáleně tvorbě. Už během práce začínají panenky a panáčci ožívat a s tím vzniká nový prostor pro hru, říše fantazie  se otevírá. \r\n \r\nNaše Viviána ukáže všem účastníkům techniku, vysvětlí postup a ráda pomůže se základními dovednostmi šití. K tvorbě používáme různobarevné nitě, krásné látky, vlny, ovčí rouno. Během práce se rozvíjí jemná motorika a cit pro estetiku, především je však panenkování krásná kreativní činnost. Přijďte si užít chvilky tvorby a pohody, potěšit srdce, odreagovat se a užít si spoustu zábavy. Vlastnoručně vyrobená panenka je krásným dárkem jak pro vás samotné, tak pro vaše milované.\r\n', '', 'Kurz je pro účastníky od 6-ti let. Mladší děti po osobní domluvě s doprovodem.', '700,-', 'Pití, svačinku a dobrou náladu', 'todo', 'Vlastní panenka je autoportrétem naší duše', NULL),
(6, 'Pružinový šperk', 'pruzinovy-sperk', 'Pomocí spirálkovače si připravíme pružiny různých velikostí a barev, které spolu s korálky a bižuterními nýty budeme postupně sestavovat a a kombinovat, až vznikne velmi originální a netradiční šperk. K výrobě šperků vám bude k dispozici celé mé korálkové království, ve kterém se nachází stovky druhů různých rokajlů, korálků dřevěných, skleněných, kovových, kabošonů, knoflíků, takže o výběr určitě nebude nouze. Jediným problémem zůstane, jaké komponenty si pro svůj vysněný šperk vybrat?', 'Výsledkem kurzu bude souprava skládající se z naušnic, náhrdelníku a náramku. \r\n', NULL, '500,-', 'Dobrou náladu', 'todo', 'Doooooong', 'Pavlína Pecko'),
(7, 'Workshop patinování', 'workshop-patinovani', 'S pomocí tavné pistole, provázků, hřebíků, gumových těsnění, větví, montážních lepidel a dalších věcí, co nám přijdou pod ruku, vytvoříme na rámu i krabičce 3D dekor. Pod tahy štětce s barvou a následně s patinovací pastou budeme pozorovat zrození krásného plastického vzoru.  \r\n', 'Techniku si vyzkoušíme na kovové krabičce a nabyté zkušenosti poté využijeme na malém zrcadélku s rámem. Obojí s vámi samozřejmě poputuje domů a tak dva dary jsou hned na světě. :) ', NULL, '700,-', 'Dobrou náladu', 'todo', 'Je to kov? Není to kov?', 'Pavlína Pecko'),
(8, 'Cínování a vitrážový šperk', 'cinovani-a-vitrazovy-sperk', 'Sklo a kov je nádherná a velmi silná kombinace. V rámci tohoto kurzu se seznámíte se základy bezpečné práce se sklem a s technikou cínování. Nabyté znalosti hned využijete v praxi při přípravě tiffany vitráže, libovolných šperků či kouzelných drobných ozdob. Inspiraci se meze nekladou a u ŠAMAMEK to platí dvojnásob. \r\n', 'Vitrážový šperk či ozdobu/dekoraci do bytu. Ocínovat si můžete i různé přírodniny, drahé kameny a jiné poklady, které se rázem po přidělání poutka rázem stanou drahocennými a originálními šperky.', 'Kurz je určen pro děti a dospělé od 14-ti let. (Mladší po dohodě :)', '600,-', 'Dobrou náladu', 'todo', 'Ze všeho se dá udělat poklad!', 'MiCha'),
(9, 'Facepainting - základy', 'facepainting-zaklady', 'Pojďte se zmalovat! Tedy. Ehm. Srdečně vás zveme na základní kurz malování na obličej!\r\n \r\nNa kurzu vás naše MiCha provede základními znalostmi a technikami potřebnými k objevování barevného světa masek a dekorací na obličej i tělo. Během tří hodin se seznámíte se základními potřebami a příslušenstvím, s jednotlivými styly a technikami používání barev. Vše si na sobě hned vyzkoušíme v praxi. Kurz zakončíme přípravou a realizací vlastního motivu.\r\n \r\nPo absolvování kurzu budete vybaveni znalostmi a dovednostmi jak připravit sebe a ostatní na tu nejdivočejší párty, nebo vaše dítka na dětskou oslavu plnou her a fantazie.\r\n \r\nSvět barev, masek, převleků a fantazie na vás již čeká!', '', 'Kurz je určen pro děti a dospělé od 14-ti let. (Mladší po dohodě :)', '500,-', 'Dobrou náladu', 'todo', NULL, NULL),
(10, 'Upcycling!', 'upcycling', 'Už jste se seznámily s novým trendem, takzvaným kreativním zrecyklováním rozbitých, starých či jinak nepotřebných věcí a jejich povznesení na úplně jinou úroveň? Ještě ne? Tak pojďte upcyklovat s námi! Na kurz vedený naší MiChou si buďto doneste vlastní poklady, pro které společně vymyslíme jejich nový účel a zrealizujeme jejich proměnu, nebo si směle vyberte z naší kreativní krabice pokladů až na místě! Škála je široká, vzniknout může cokoli od šperku až po vkusný bytový doplněk. ', '', 'Kurz je pro účastníky od 14-ti let. (Mladší po dohodě :)', '500,-', 'Dobrou náladu', 'todo', NULL, NULL),
(11, 'Jak na osobní přírodní parfém', 'jak-na-osobni-prirodni-parfem', '"Hmm, kdo to tu tak krásně voní?" Tuhle větu budete po absolvování kurzu slýchat velmi často, veeelmi často.\r\n \r\nPonořte se do světa kvalitních 100% přírodních vůní a naučte se, jak si vyrobit svůj osobní parfém přímo na míru. Zapomeňte jednou pro vždy na ty předražené syntetické preparáty plné chemikálií a hýčkejte se přírodou! Váš nový parfém nebude pouze vůní, ale aromaterapeutickým společníkem po celý den.\r\n \r\nV rámci kurzu si sice představíme něco z teorie parfumářského umění, naším hlavním společníkem při tvrobě nám však bude naše INTUICE a náš nos. Na základě intuitivního výběru základních esencí (z nabídky vice než 30 kvalitních přírodních esenciálních olejů) budeme společně ladit tu správnou, unikátní a osobitou kompozici.', '5ml toho nejúžasněji vonícího osobního olejového parfému, v minimalistickém a kompaktním balení, který vám bude dělat společnost několik měsíců (a znovu si ho pak s nabytými znalostmi vyrobíte již sami :)).', 'Pro koho je kurz určen: Pro ženy muže a děti od 14. let (mladší po dohodě)', '600 Kč', 'Radost, nos a intuici. ', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `roleName`) VALUES
(1, 'fasdfsd');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `email`, `role`) VALUES
(1, 'superadmin', 'toxon@seznam.cz', '$2y$10$DwK4IhmA8sNgBXCBfFcd/.rtTUuqx/d.ynXuSNMchnctO5PlOFIBK', 'toxon@seznam.cz', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`role`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
