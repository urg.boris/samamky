<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
		$router[] = new Route('admin', 'Admin:Homepage:default');
		$router[] = new Route('<presenter>/<action>[/<id>]', 'KdoJsme:default');
                //$router[] = new Route('[/]', 'KdoJsme:default');
                //$router[] = new Route('admin/<presenter>/<action>[/<id>]', 'Admin:Homepage:default');
                //$router[] = new Route('kurzy/detail/<courseName>', 'Kurzy:detail ');
		return $router;
	}

}
