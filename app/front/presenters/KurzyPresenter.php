<?php

namespace App\Presenters;

use Nette;
use Nette\Utils\Strings;


class KurzyPresenter extends Nette\Application\UI\Presenter
{
    
    /** @var Nette\Database\Context @inject */
    public $database;
	
    /** @inject @var \App\Model\ImageStorage */
    public $imageStorage;
    
    /** @inject @var \App\Model\CourseLister */
    public $courseLister;

    
    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->courses = $this->database->table('courses')->where('active = ?', 1);
    }
    
    public function renderDetail($id)
    {
        $this->template->courseInfo = $this->database->table('courses')->where('url = ?', $id)->limit(1)->fetch();
        $this->template->events = $this->courseLister->listCourses($id);
        $this->template->imageDir = "kurzy/$id";
	$this->template->images = $this->imageStorage->getImagesFromDir($this->template->imageDir);
    }
}
