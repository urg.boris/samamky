<?php

namespace App\Presenters;

use Nette;


class KdoJsmePresenter extends Nette\Application\UI\Presenter
{
	/** @inject @var \App\Model\ImageStorage */
    public $imageStorage;
	
	private $artistDir = "";
	
	public function actionPavlinaPecko() {
		$this->template->artistDir = 'pavlina-pecko';
		$this->template->images = $this->imageStorage->getImagesFromDir($this->template->artistDir);
	}
	
	public function actionMiCha() {
		$this->template->artistDir = 'micha';
		$this->template->images = $this->imageStorage->getImagesFromDir($this->template->artistDir);
	}
	
	public function actionVivianaNaLiane() {
		$this->template->artistDir = 'viviana-na-liane';
		$this->template->images = $this->imageStorage->getImagesFromDir($this->template->artistDir);
	}
	
//	public function actionIrenaLiberdova() {
//		$this->template->artistDir = 'irena-liberdova';
//		$this->template->images = $this->imageStorage->getImagesFromDir($this->template->artistDir);
//	}
//	
//	public function actionPetraTumova() {
//		$this->template->artistDir = 'petra-tumova';
//		$this->template->images = $this->imageStorage->getImagesFromDir($this->template->artistDir);
//	}
}
