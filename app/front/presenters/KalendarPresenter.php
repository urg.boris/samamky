<?php

namespace App\Presenters;

use Nette;
use Google_Client;
use Google_Service_Drive;

class KalendarPresenter extends Nette\Application\UI\Presenter
{
    /** @var Nette\Database\Context @inject */
    public $database;
    
    /** @inject @var \App\Model\CourseLister */
    public $courseLister;
  

    
    public function actionDefault() {
        try {
            $this->template->events = $this->courseLister->listCourses();
        } catch(\Exception $e) {
            $this->template->events = [];
        }
    }
}
