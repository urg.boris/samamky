<?php
namespace App\Model;

use Nette;
use Nette\Security\Permission;
use Nette\Security as NS;
use Nette\Database\Context;

class UserAuthenticator implements NS\IAuthenticator
{
    use Nette\SmartObject;
    
    public $database;
	
    const TABLE_NAME = 'users';
    const USERNAME_COLUMN = 'username';
    const SUPERADMIN = 'superadmin';
    const ADMIN = 'admin';
    const NOBODY = 'nobody';

    function __construct(Context $database) {
        $this->database = $database;
        //TODO do databaze
/*
        $acl = new Permission;

        // roles definition
        $acl->addRole(self::NOBODY);
        $acl->addRole(self::ADMIN , self::NOBODY);
        $acl->addRole(self::SUPERADMIN, self::ADMIN);
        
        $acl->addResource('articles');

        $acl->allow(self::NOBODY, Permission::ALL, 'view');
        
        $acl->addResource('admin');

        $acl->allow(self::ADMIN, Permission::ALL, ['view', 'edit', 'add']);

        //allow all
        $acl->allow(self::SUPERADMIN);
 * 
 */
    }

    function authenticate(array $credentials)
    { 
        list($username, $password) = $credentials;
        $row = $this->database->table(self::TABLE_NAME)
            ->where(self::USERNAME_COLUMN, $username)->fetch();
        if (!$row) {
            throw new NS\AuthenticationException('User not found.');
        }
        if (!NS\Passwords::verify($password, $row->password)) {
            throw new NS\AuthenticationException('Invalid password.');
        }
        
        return new NS\Identity($row->id, $row->role, array('name' => $row->name));
    }
}
class UserAuthorizator implements NS\IAuthorizator {
    use Nette\SmartObject;
    function isAllowed($role, $resource, $privilege) {
        return true;
    }

}