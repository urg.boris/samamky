<?php

namespace App\Model;

use Nette;
use Google_Client;
use Google_Service_Drive;

class CourseLister {
    
    use Nette\SmartObject;
    
    /** @var array */
    private $coursesParams;
    
    private $googleService;
    
    private $googleAPIClientSecretFile = __DIR__ . '/../config/googleapi/calendar-access.json';
    
    private $entries = [
        "date" => "entry.617807374",
        "formAnswer" => "entry.1593896653",
    ];
    
    private $daysInWeek = ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'];
    
    private $nowFormat;
    
    private $returnedEvents;
    /**
     * @var Nette\Database\Context
     */
    private $database;

    public function __construct(array $params, Nette\Database\Context $database)
    {
        $this->coursesParams = $params;
        $this->database = $database;
    }
    
    private function setEnviroment() {
        $client = new Google_Client();

        $client->setAuthConfig($this->googleAPIClientSecretFile);
        $client->setAccessType('offline');
        $client->setApplicationName("Samamky");

        $client->setScopes([\Google_Service_Calendar::CALENDAR_READONLY]);

        $this->googleService = new \Google_Service_Calendar($client);
        $now = new \DateTime;
        $this->nowFormat = $now->format(\DateTime::RFC3339);
        
        $this->returnedEvents = [];
    }
    
    public function listCourses($id = null) {
        $returnedEvents = [];
        try {
            $this->setEnviroment();

            if($id) {
                return $this->getCourseEventsById($id);
            } else {
                foreach ($this->coursesParams["courses"] as $key => $course) {
                    $returnedEvents += $this->getCourseEventsById($key);                
                }
                ksort($returnedEvents); 
                return $returnedEvents;
            }
        } catch(\Exception $e) {
            return [];
        }
    }
    private function getCourseEventsById(string $id): array {
        $events = [];
        $returnedEvents = [];

        $dateTime = null;
        $endDateTime = null;
        $prefill = null;        
        
        if(!isset($this->coursesParams["courses"][$id])) {
            throw new \RuntimeException("course not found $id");
        }
        $course = $this->coursesParams["courses"][$id];

        //todo pole check
        $events = $this->googleService->events->listEvents($course["calendar"], [
            'timeMin' => $this->nowFormat,
            ])->getItems();
        
        foreach ($events as $eKey => $eValue) {
          
            $dateTime = new \DateTime($eValue["start"]["dateTime"]);
            $endDateTime = new \DateTime($eValue["end"]["dateTime"]);
            $endHour = $endDateTime->format('H:i');

            $prefill = null;
            $prefill[$this->entries["date"]] = $dateTime->format('Y-m-d');
            $prefill[$this->entries["formAnswer"]] = $course["formAnswer"];
            $returnedEvents[$dateTime->getTimestamp()] = [
                "course" => $id,
                "name" => $eValue["summary"],
                "where" => $eValue["location"],
                "formatDate" => $dateTime->format('d.m.Y H:i')."-".$endHour." (".$this->daysInWeek[(integer)$dateTime->format('w')].")",
                "description" => $eValue["decription"],
                "courseName" => $this->database->table('courses')->where('url', $id)->fetchField('name'),
                "registerLink" => $this->coursesParams["registerLink"] . http_build_query($prefill),
                ];
        }  
         
        return $returnedEvents;
    }
}
