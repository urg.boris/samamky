<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;
use Nette\Utils\DateTime;

/**
 * Users management.
 */
class UserManager extends Nette\Object implements Nette\Security\IAuthenticator {

    const
            TABLE_NAME = 'users',
            COLUMN_ID = 'id',
            COLUMN_NAME = 'name',
            COLUMN_PASSWORD_HASH = 'pass',
            COLUMN_ROLE = 'role',
            COLUMN_CREATED = 'created',
            COLUMN_UPDATE = 'update';

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    /**
     * Performs an authentication.
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     */
    public function authenticate(array $credentials) {
        list($username, $password, $role) = $credentials;

        $row = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_NAME, $username)->fetch();

        if (!$row) {
            throw new Nette\Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);
        } elseif (!Passwords::verify($password, $row[self::COLUMN_PASSWORD_HASH])) {
            throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);
        } elseif (Passwords::needsRehash($row[self::COLUMN_PASSWORD_HASH])) {
            $row->update(array(
                self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
            ));
        }

        $arr = $row->toArray();
        unset($arr[self::COLUMN_PASSWORD_HASH]);
        return new Nette\Security\Identity($row[self::COLUMN_ID], $row[self::COLUMN_ROLE], $arr);
    }

    /**
     * Adds new user.
     * @param  string
     * @param  string
     * @return void
     */
    public function add($adminName, $password, $role) {
        try {
            $row = $this->database->table(self::TABLE_NAME)->insert(array(
                self::COLUMN_NAME => $adminName,
                self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
                self::COLUMN_ROLE => $role,
                self::COLUMN_CREATED => new DateTime,
                self::COLUMN_UPDATE => new DateTime,
            ));
            return $row;
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException('Duplicate admin name.');
        }
    }

}

class DuplicateNameException extends \Exception {
    
}
