<?php

namespace App\Model;

use Nette;
use Nette\Utils\Finder;
use Nette\Utils\Image;


class ImageStorage
{
    
    use Nette\SmartObject;
    
    private $wwwDir;

    public function __construct(string $wwwDir)
    {
        $this->wwwDir = $wwwDir;
    }

//    public function save($file, $contents)
//    {
//        file_put_contents($this->dir . '/' . $file, $contents);
//    }
	
	public function getImagesFromDir(string $imageDir) {
		$images = [];
                $image = null;
		$dir = $this->wwwDir . DIRECTORY_SEPARATOR . $imageDir;
		if(!is_dir($dir)) {
			throw new \RuntimeException("directory $dir not found.");
		}
		foreach (Finder::findFiles('*.jpg', '*.jpeg', '*.gif', '*.png')->in($dir) as $key => $file) {
                    $images[] = $file->getFilename();
                    $thumbFileName = $dir . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $file->getFilename();

                    if(!file_exists($thumbFileName)) {
                        chmod($dir . DIRECTORY_SEPARATOR . 'thumb', 0755);
                        $image = Image::fromFile($file);                        
                        $image->resize(150, 150, Image::SHRINK_ONLY);                        
                        $image->save($thumbFileName, 100);
                    }
		}
		return $images;
	}
}
