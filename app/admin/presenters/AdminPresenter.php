<?php

namespace App\AdminModule\Presenters;

use Nette;
use Nette\Application\UI\Form;


class AdminPresenter extends Nette\Application\UI\Presenter
{
    /** @var \Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }
    
    public function startup() {
        parent::startup();
        if(!$this->getUser()->isLoggedIn()) {
            if(!$this->isLinkCurrent("PrihlasSe:Default")) {
                $this->redirect("PrihlasSe:Default");
            }
        } else {
            $this->template->name = $this->getUser()->getIdentity()->name;
        }
    }
    
    
    public function actionPrihlasSe() {
        
    }

   protected function createComponentPrihlasSeForm() {
        $form = new Nette\Application\UI\Form;
        $form->addText('username', 'Uživatelské jméno:')
                ->setRequired('Prosím vyplňte své uživatelské jméno.');

        $form->addPassword('password', 'Heslo:')
                ->setRequired('Prosím vyplňte své heslo.');

        $form->addCheckbox('remember', 'Zůstat přihlášen');

        $form->addSubmit('send', 'Přihlásit');

        $form->onSuccess[] = array($this, 'signInFormSucceeded');
        return $form;
    }

    public function actionVen() {
        $this->getUser()->logout();
        $this->flashMessage('Odhlášen.');
        $this->redirect('Admin:PrihlasSe:Default');
    }

    public function signInFormSucceeded($form) {
        $values = $form->values;

        try {
            $user = $this->getUser();
            $user->setAuthorizator(new UserAuthorizator);
            $user->login($values->username, $values->password);
            $this->redirect('Admin:default');
        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError('Nesprávné přihlašovací jméno nebo heslo.');
        }
    }
    
    public function renderDetail($id)
    {
        $this->template->courseInfo = $this->database->table('courses')->where('url = ?', $id)->limit(1)->fetch();
    }
    
    
    protected function createComponentUploadImage()
    {
        $form = new Form;
        $form->addText('name', 'Jméno obrázečku: ')
             ->setRequired('Zadej jméno souboru prosimtě');
        $form->addUpload('image', 'Obrázeček v kompjůtru:')
            ->setRequired('Vyber ňákej obrázek...')
            ->addRule(Form::IMAGE, 'Tenhle obrázek neumim nahrát. Měl by mít příponu JPG, PNG nebo GIF');
        $form->addSubmit('submit', 'Uložit');
        $form->onSuccess[] = [$this, 'onSuccess'];
        return $form;
    }

    // called after form is successfully submitted
    public function onSuccess(Form $form, $values)
    {
        /** @var \Nette\Http\FileUpload */
        $image = $values['image'];
        if(!$image->isImage()) {
            //TODO
        }
        
        
    }
}
