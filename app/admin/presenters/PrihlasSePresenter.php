<?php

namespace App\AdminModule\Presenters;

use Nette;
use Nette\Application\UI\Form;
use App\Model\UserAuthenticator;
use App\Model\UserAuthorizator;
use App\Model\UserManager;
use Nette\Security\Passwords;

class PrihlasSePresenter extends Nette\Application\UI\Presenter {
    /** @var \Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    protected function createComponentPrihlasSeForm() {
        $form = new Nette\Application\UI\Form;
        $form->addText('username', 'Uživatelské jméno:')
                ->setRequired('Prosím vyplňte své uživatelské jméno.');

        $form->addPassword('password', 'Heslo:')
                ->setRequired('Prosím vyplňte své heslo.');

        $form->addCheckbox('remember', 'Zůstat přihlášen');

        $form->addSubmit('send', 'Přihlásit');

        $form->onSuccess[] = array($this, 'signInFormSucceeded');
        return $form;
    }

    public function actionVen() {
        $this->getUser()->logout();
        $this->flashMessage('Odhlášen.');
        $this->redirect('PrihlasSe:default');
    }

    /**
     * 
     * @param Nette\Application\UI\Form $form
     */
    public function signInFormSucceeded($form) {
        $values = $form->values;

        try {
            $user = $this->getUser();
            $user->setAuthorizator(new UserAuthorizator);
            $user->login($values->username, $values->password);
            $this->redirect('Admin:default');
        } catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage('Nesprávné přihlašovací jméno nebo heslo.');
            //$form->addError('Nesprávné přihlašovací jméno nebo heslo.');
        }
    }
}
