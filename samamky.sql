-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mamaId` int(11) DEFAULT NULL,
  `active` varchar(255) COLLATE utf8_czech_ci DEFAULT '1',
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` text COLLATE utf8_czech_ci NOT NULL,
  `takeAway` text COLLATE utf8_czech_ci,
  `note` text COLLATE utf8_czech_ci,
  `price` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `equipment` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `registerLink` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `quote` text COLLATE utf8_czech_ci,
  `quotator` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`),
  KEY `mamaId` (`mamaId`),
  CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`mamaId`) REFERENCES `mamas` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `courses` (`id`, `mamaId`, `active`, `name`, `url`, `description`, `takeAway`, `note`, `price`, `equipment`, `registerLink`, `quote`, `quotator`) VALUES
(2,	1,	'1',	'Keramická mozaika',	'keramicka-mozaika',	'Naučte se techniku, pomocí které oživíte kusy nábytku, stěnu nebo podlahu. V kurzu si vyzkoušíte různé způsoby vytváření střepů z obkladů a naučíte se lepení a spárování na keramickém květináči, samostatném mozaikovém obraze nebo na rámu zrcadla.\r\n\r\nMozaika je spojením prožitku z tvorby s estetickým a funkčním výsledkem.',	'',	'Veškeré pomůcky a materiál jsou v ceně kurzu.',	'700,-',	'',	'todo',	NULL,	NULL),
(3,	1,	'1',	'Beads mozaika',	'beads-mozaika',	'Beads mozaika je technika, jejímž základním prvkem je využití různorodých skleněných korálků, knoflíků a kabošonů, kterými budeme dekorovat dřevěné šperkovnice, krabičky nebo dózy.\r\nTato technika je pracnější než klasické keramické mozaikování, práce s pinzetou je celkově jemnější a detailnější.',	'',	'Veškeré pomůcky a materiál jsou v ceně kurzu, který je celodenní s hodinovou pauzou na oběd.',	'2500,-',	'',	'todo',	NULL,	NULL),
(4,	1,	'1',	'Mikromozaika',	'mikromozaika',	'Kurz mikromozaiky je “ochutnávkou” beads mozaiky. \r\nZ korálků, knoflíků a kabošonů různých velikostí a barev sivytvoříte originální brož, náušnice, náhrdelník nebo prsten.\r\n\r\nTato technika je pracnější než klasická keramická mozaika, práce s pinzetou je jemnější a detailnější. \r\n',	'',	'Veškeré pomůcky a materiál jsou v ceně kurzu.',	'600,-',	'',	'todo',	NULL,	NULL),
(5,	2,	'1',	'Panenkování s Viviánou na liáně',	'panenkovani-s-vivianou-na-liane',	'V rámci panenkování s Vivianou na liáně si každý vyrobí svou osobní panenku, panáčka, hrdinu či hrdinku. Děti i dospělí si v ateliéru, kde je vřelá a přátelská atmosféra, užívají doslova každou minutu. Celé 4 hodiny se věnují velmi zapáleně tvorbě. Už během práce začínají panenky a panáčci ožívat a s tím vzniká nový prostor pro hru, říše fantazie  se otevírá. \r\n \r\nNaše Viviána ukáže všem účastníkům techniku, vysvětlí postup a ráda pomůže se základními dovednostmi šití. K tvorbě používáme různobarevné nitě, krásné látky, vlny, ovčí rouno. Během práce se rozvíjí jemná motorika a cit pro estetiku, především je však panenkování krásná kreativní činnost. Přijďte si užít chvilky tvorby a pohody, potěšit srdce, odreagovat se a užít si spoustu zábavy. Vlastnoručně vyrobená panenka je krásným dárkem jak pro vás samotné, tak pro vaše milované.\r\n',	'',	'Kurz je pro účastníky od 6-ti let. Mladší děti po osobní domluvě s doprovodem.',	'700,-',	'Pití, svačinku a dobrou náladu',	'todo',	'Vlastní panenka je autoportrétem naší duše',	NULL),
(6,	NULL,	'0',	'Pružinový šperk',	'pruzinovy-sperk',	'Pomocí spirálkovače si připravíme pružiny různých velikostí a barev, které spolu s korálky a bižuterními nýty budeme postupně sestavovat a a kombinovat, až vznikne velmi originální a netradiční šperk. K výrobě šperků vám bude k dispozici celé mé korálkové království, ve kterém se nachází stovky druhů různých rokajlů, korálků dřevěných, skleněných, kovových, kabošonů, knoflíků, takže o výběr určitě nebude nouze. Jediným problémem zůstane, jaké komponenty si pro svůj vysněný šperk vybrat?',	'Výsledkem kurzu bude souprava skládající se z naušnic, náhrdelníku a náramku. \r\n',	NULL,	'500,-',	'Dobrou náladu',	'todo',	'Doooooong',	'Pavlína Pecko'),
(7,	1,	'1',	'Kurz patinování',	'kurz-patinovani',	'Na plechovou krabičku, dřevěné zrcadlo nebo rámeček vytvoříme plastický dekor pomocí tavné pistole, provázků, hřebíků, gumových těsnění, větví, zipů, gázy apod. Poté natřeme barvou a budeme pozorovat jak pod štětcem s patinou ožívá váš nový doplněk do interiéru nebo třeba budoucí dárek.\r\n',	'',	'Veškeré pomůcky a materiál jsou v ceně kurzu.',	'700,-',	'',	'todo',	'',	''),
(8,	3,	'1',	'Tiffany technika',	'tiffany-technika',	'Sklo a kov je nádherná a velmi silná kombinace.\r\n\r\nV rámci tohoto kurzu se seznámíte se základy bezpečné práce se sklem a s technikou cínování. Nabyté znalosti hned využijete v praxi a domů si odnesete krásný šperk či milou ozdobu/dekoraci. \r\n\r\nInspiraci se meze nekladou a u ŠAMAMEK to platí dvojnásob. \r\n\r\n',	'',	'Veškeré pomůcky a materiál jsou v ceně kurzu.',	'700,-',	'',	'todo',	'Ze všeho se dá udělat poklad!',	'MiCha'),
(9,	3,	'1',	'Facepainting - základy',	'facepainting-zaklady',	'Srdečně vás zveme na základní kurz malování na obličej! Na kurzu vás naše MiCha provede základními znalostmi a technikami potřebnými k objevování barevného světa masek a dekorací na obličej i tělo. Během tří hodin se seznámíte se základními potřebami a příslušenstvím, s jednotlivými styly a technikami používání barev. Vše si na sobě hned vyzkoušíme v praxi. Kurz zakončíme přípravou a realizací vlastního motivu. Po absolvování kurzu budete vybaveni znalostmi a dovednostmi jak připravit sebe a ostatní na tu nejdivočejší párty, nebo vaše dítka na dětskou oslavu plnou her a fantazie. Svět barev, masek, převleků a fantazie na vás již čeká!',	'',	'Veškeré pomůcky a materiál jsou v ceně kurzu.',	'500,-',	'',	'todo',	NULL,	NULL),
(10,	3,	'1',	'Upcycling!',	'upcycling',	'Už jste se seznámili s novým trendem, takzvaným kreativním zrecyklováním rozbitých, starých či jinak nepotřebných věcí a jejich povznesení na úplně jinou úroveň? Ještě ne? Tak pojďte upcyklovat s námi! Na kurz vedený naší MiChou si buďto doneste vlastní poklady, pro které společně vymyslíme jejich nový účel a zrealizujeme jejich proměnu, nebo si směle vyberte z naší kreativní krabice pokladů až na místě! Škála je široká, vzniknout může cokoli od šperku až po vkusný bytový doplněk.',	'',	'Veškeré pomůcky a materiál jsou v ceně kurzu.',	'700,-',	'Dobrou náladu',	'todo',	NULL,	NULL),
(11,	NULL,	'0',	'Jak na osobní přírodní parfém',	'jak-na-osobni-prirodni-parfem',	'\"Hmm, kdo to tu tak krásně voní?\" Tuhle větu budete po absolvování kurzu slýchat velmi často, veeelmi často.\r\n \r\nPonořte se do světa kvalitních 100% přírodních vůní a naučte se, jak si vyrobit svůj osobní parfém přímo na míru. Zapomeňte jednou pro vždy na ty předražené syntetické preparáty plné chemikálií a hýčkejte se přírodou! Váš nový parfém nebude pouze vůní, ale aromaterapeutickým společníkem po celý den.\r\n \r\nV rámci kurzu si sice představíme něco z teorie parfumářského umění, naším hlavním společníkem při tvrobě nám však bude naše INTUICE a náš nos. Na základě intuitivního výběru základních esencí (z nabídky vice než 30 kvalitních přírodních esenciálních olejů) budeme společně ladit tu správnou, unikátní a osobitou kompozici.',	'5ml toho nejúžasněji vonícího osobního olejového parfému, v minimalistickém a kompaktním balení, který vám bude dělat společnost několik měsíců (a znovu si ho pak s nabytými znalostmi vyrobíte již sami :)).',	'Pro koho je kurz určen: Pro ženy muže a děti od 14. let (mladší po dohodě)',	'600 Kč',	'Radost, nos a intuici. ',	'',	NULL,	NULL),
(12,	3,	'1',	'Upcyklovaný šperk Tiffany technikou',	'upcyklovany-sperk-tiffany-technikou',	'Sklo, porcelán, přírodniny a kov je nádherná a velmi silná kombinace. V rámci tohoto kurzu se seznámíte se základy práce se sklem, porcelánem, přírodninami, pájkou a cínem, tedy s technikou Tiffany. Naučíme se sklo, porcelán řezat, štípat i brousit do požadovaných i nepožadovaných:) tvarů.\r\n\r\nNabyté znalosti hned využijete v praxi při přípravě libovolných šperků či kouzelných drobných ozdob.\r\n\r\nInspiraci se meze nekladou!',	NULL,	'Veškeré pomůcky a materiál jsou v ceně kurzu. ',	'700,-',	NULL,	'',	NULL,	NULL),
(13,	1,	'1',	'Dekorování porcelánu mozaikou',	'dekorovani-porcelanu-mozaikou',	'Porcelánové konvičky, cukřenky nebo dózy se rozzáří s novým mozaikovým dekorem tvořeným skleněnými korálky, kabošony nebo knoflíky.\r\nTato technika je pracnější než klasické keramické mozaikování, práce s pinzetou je jemnější a detailnější. \r\n',	'Z kurzu si odnesete upcyklovaný porcelán dle výběru. Je možné si přinést i vlastní k přetvoření.',	'Veškeré pomůcky a materiál jsou v ceně kurzu, který je celodenní s hodinovou pauzou na oběd.',	'2500,-',	NULL,	'',	NULL,	NULL),
(14,	3,	'1',	'Proprietník - upcyklovaná mozaika',	'proprietnik-upcyklovana-mozaika',	'Z nevyužité keramiky či porcelánu postupně skládáme a sestavujeme svůj osobní Proprietník zrcadlo nebo Proprietník obraz. S touto upcyklovanou mozaikou hravě a vtipně uspořádáte své zbytnosti i nezbytnosti u vás doma.\r\n\r\nBěhem kurzu si vyzkoušíte různé způsoby vytváření střepů z obkladů, keramiky a porcelánu a naučíte se lepení a spárování.\r\n\r\nKurz je dvoudenní. \r\nSobota od 10:00 do 18:00 s hodinovou pauzou na oběd - lepení střepů.\r\nNeděle od 13:00 do 16:00 - spárování.',	NULL,	'Veškeré pomůcky a materiál jsou v ceně kurzu. \r\nMaximální počet účastníků workshopu je 5 osob. \r\n',	'4500',	NULL,	'',	NULL,	NULL);

DROP TABLE IF EXISTS `mamas`;
CREATE TABLE `mamas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `linkId` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `mamas` (`id`, `name`, `linkId`) VALUES
(1,	'Pavlína Pecko',	'pavlina-pecko'),
(2,	'Viviána na liáně',	'viviana-na-liane'),
(3,	'MiCha',	'mi-cha');

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `roles` (`id`, `roleName`) VALUES
(1,	'fasdfsd');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`),
  CONSTRAINT `users_ibfk_3` FOREIGN KEY (`role`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `users` (`id`, `name`, `username`, `password`, `email`, `role`) VALUES
(1,	'superadmin',	'toxon@seznam.cz',	'$2y$10$DwK4IhmA8sNgBXCBfFcd/.rtTUuqx/d.ynXuSNMchnctO5PlOFIBK',	'toxon@seznam.cz',	1);

-- 2018-03-17 20:15:54
